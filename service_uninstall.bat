@echo off
rem 停止mysql服务（如果存在）
net stop mysql
rem @echo off的作用是关闭回显功能

rem 以下测试路径
echo 当前盘符：%~d0 
echo 当前盘符和路径：%~dp0 
echo 当前批处理全路径：%~f0 
echo 当前盘符和路径的短文件名格式：%~sdp0 
echo 当前CMD默认目录：%cd% 

set mypath=%~dp0
set mypath=%mypath:\=\\%
echo 批处理文件所在路径转为双斜杠短名: %mypath%
echo 当前批处理正工作的路径转换后: %cd:\=\\%


rem 找到当前路径
rem 将temp.txt文件中内容拷贝到temp.bat文件中
rem temp.txt文件中的内容是“set PathTemp=”
rem 特别注意：“=”后面不能加回车
copy temp.txt  temp.bat

rem 将路径添加到temp.bat文件中
cd>>temp.bat

rem 执行temp.bat文件中命令
rem temp.bat文件执行后，路径已经保存在PathTemp变量中
call temp.bat

rem 删除temp.bat文件
rem del temp.bat

rem 设置运行环境变量homedir
set homedir=%PathTemp%\bin

rem 设置变量延迟
rem 注意：使用变量延迟后，相关变量用！！包围，这样程序才能正确识别
setlocal enabledelayedexpansion

rem TIMESTAMP with implicit DEFAULT value is deprecated
rem 设置删除mysql服务的命令
set uninstallcmd=.\bin\mysqld -remove
@rem 调用删除命令
call !uninstallcmd!

@pause